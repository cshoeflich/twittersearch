﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Services;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    [WebMethod]
    public static string TwitterSearch(string searchText)
    {
        searchText = HttpUtility.UrlEncode("#" + searchText);

        WebRequest request = WebRequest.Create("https://api.twitter.com/1.1/search/tweets.json?q=" + searchText + "&result_type=popular");
        request.Headers.Add("Authorization:Bearer AAAAAAAAAAAAAAAAAAAAAHJpzQAAAAAAqfN6nLyek0aafwivygSdCMa5TE8%3D9noklodvNDQ5PeWWMGOoNEp4QonoQ3bALunpRMMvcEwFmVnRN3");

        WebResponse response = request.GetResponse();
        Stream dataStream = response.GetResponseStream();
        StreamReader reader = new StreamReader(dataStream);
        string responseText = reader.ReadToEnd();

        reader.Close();
        response.Close();

        return responseText;
    }
}